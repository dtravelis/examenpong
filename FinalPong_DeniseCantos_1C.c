/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [STUDENT NAME HERE]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

typedef enum GameScreen { LOGO, TITLE, GAMEPLAY, ENDING } GameScreen;

int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
    int screenWidth = 800;
    int screenHeight = 450;
    char windowTitle[30] = "raylib game - FINAL PONG";
    InitWindow(screenWidth, screenHeight, windowTitle);
    
    InitAudioDevice();
    
    GameScreen screen = LOGO;
    
    // TODO: Define required variables here..........................(0.5p)
    // NOTE: Here there are some useful variables (should be initialized)
    
    //Sonidos
    Sound enterSound = LoadSound("Resources/enter.ogg");
    Sound pointSound = LoadSound("Resources/point.ogg");
    Sound collisionSound = LoadSound("Resources/collision.ogg");
    Sound winSound = LoadSound("Resources/win.ogg");
    Sound empateSound = LoadSound("Resources/empate.ogg");
    Sound loseSound = LoadSound("Resources/loser.ogg");
    Music gameplayMusic = LoadMusicStream("Resources/gameplay.ogg");
    
    //Logo
    Texture2D logo = LoadTexture("Resources/logo.png");
    float alpha = 0.0f;
    bool fadeIn = true;
    
    //Título
    Font arcadeFont = LoadFont("Resources/arcadefont.ttf");
    char titleText [14] = "FINAL PONG";
    char startText [14] = "press enter";
    int titleSize = 50;
    Vector2 titlePos = {screenWidth/2 - MeasureTextEx(arcadeFont, titleText, titleSize, 0).x/2, -100 };
    bool blink = false;
    
    Texture2D pantInicio = LoadTexture("Resources/pantallainicio.png");
        
    //Gameplay
    Texture2D campo = LoadTexture("Resources/campo.png");
    
    Color ballColor = {237,33,6,255};
    Color playerColor = {144,192,208,255};
    Color playerLifeColor = {237,153,6,255};
    
    
    float topMargin = 50;
    float counterMargin = 25;
    float lifeMargin = 5;
    
    int limiteCampo = 5;
    int playerHeight = 80;
    int playerWidth = 18;
    
    Rectangle player = {limiteCampo, screenHeight/2 - playerHeight/2 + 25, playerWidth, playerHeight};
    int playerSpeedY = 4;
    
    Rectangle enemy = {screenWidth - playerWidth - 5, screenHeight/2 - playerHeight/2 + 25, playerWidth, playerHeight};
    int enemySpeedY = 4;
    float AImargin = enemy.height/4;
    
    
    Rectangle playerBgRec = {0, 0, screenWidth/2 - counterMargin, topMargin};
    Rectangle playerFillRec = {playerBgRec.x + lifeMargin, playerBgRec.y + lifeMargin, playerBgRec.width - lifeMargin*2, playerBgRec.height - lifeMargin*2};
    Rectangle playerLifeRec = playerFillRec;
    
    Rectangle enemyBgRec = {screenWidth/2 + counterMargin, 0, screenWidth/2 - counterMargin, topMargin};
    Rectangle enemyFillRec = {enemyBgRec.x + lifeMargin, enemyBgRec.y + lifeMargin, enemyBgRec.width - lifeMargin*2, enemyBgRec.height - lifeMargin*2};
    Rectangle enemyLifeRec = enemyFillRec;
    
    Rectangle mid = {screenWidth/2 - counterMargin, 0, counterMargin*2, topMargin};
    
    
    int ballRadius = 15;
    Vector2 ballPosition = {screenWidth/2 - ballRadius, screenHeight/2 + ballRadius};
    Vector2 ballSpeed = { 3, 5};
    if(GetRandomValue(0, 1)) ballSpeed.x *= -1;
    if(GetRandomValue(0, 1)) ballSpeed.y *= -1;
    
    
    int playerLife = 5;
    int enemyLife = 5;
    
    float widthDamage = playerLifeRec.width/playerLife;
    
    int secondsCounter = 99;
    
    int framesCounter;          // General pourpose frames counter
    
    int gameResult = -1;        // 0 - Loose, 1 - Win, 2 - Draw, -1 - Not defined
    
    bool pausa = false;
    
    //Ending
    Texture2D win = LoadTexture("Resources/win.png");
    Texture2D loser = LoadTexture("Resources/loser.png");
    Texture2D empate = LoadTexture("Resources/empate.png");    
    
    
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
    
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
        switch(screen) 
        {
            case LOGO: 
            {
                // Update LOGO screen data here!
                
                // TODO: Logo fadeIn and fadeOut logic...............(0.5p)
                if (fadeIn)
                {
                    alpha += 1.0f/90;
                    
                    if (alpha >= 1.0f)
                    {
                        alpha = 1.0f;
                        
                        framesCounter++;
                        if (framesCounter % 120 == 0)
                        {
                            framesCounter = 0;
                            fadeIn = false;
                        }
                    }
                }
                else{
                    alpha -= 1.0f/90;
                    
                    if (alpha <= 0.0f)
                    {
                        //cambia a la titlescreen
                        framesCounter = 0;
                        screen = TITLE;
                    }
                }
                
                if (IsKeyPressed (KEY_ENTER))
                {
                    framesCounter = 0;
                    screen = TITLE;
                    PlaySound(enterSound);
                }
                
                
            } break;
            case TITLE: 
            {
                // Update TITLE screen data here!
                
                // TODO: Title animation logic.......................(0.5p)
                if (titlePos.y < 100)
                {
                titlePos.y +=5;
                
                }
                else{              
                                                              
                // TODO: "PRESS ENTER" logic.........................(0.5p)
                    framesCounter++;
                    if (framesCounter % 12 == 0)
                    {
                        framesCounter = 0;
                        blink = !blink;
                    }
                    if (IsKeyPressed(KEY_ENTER)){
                        framesCounter = 0;
                    PlayMusicStream(gameplayMusic);
                    screen = GAMEPLAY;
                    }
                    
                if (IsKeyPressed (KEY_ENTER)) PlaySound(enterSound);

                }
            } break;
            case GAMEPLAY:
            { 
                UpdateMusicStream(gameplayMusic);
                // Update GAMEPLAY screen data here!
                if (!pausa)
             {
                // TODO: Ball movement logic.........................(0.2p)
                ballPosition.x += ballSpeed.x;
                ballPosition.y += ballSpeed.y;
                
                // TODO: Player movement logic.......................(0.2p)
                if(IsKeyDown (KEY_UP))
                {
                    player.y -= playerSpeedY;
                }
                if(IsKeyDown (KEY_DOWN))
                {
                    player.y += playerSpeedY;
                }
                
                if (player.y < topMargin+limiteCampo)
                {
                    player.y = topMargin+5;
                }
                if (player.y > screenHeight - playerHeight-limiteCampo)
                {
                    player.y = screenHeight-playerHeight-limiteCampo;
                }
                
                // TODO: Enemy movement logic (IA)...................(1p)
                if((ballPosition.x > screenWidth/2) && ballSpeed.x > 0)
                {
                        if(ballPosition.y < enemy.y + enemy.height/2 - AImargin) enemy.y -= enemySpeedY;
                        if(ballPosition.y > enemy.y + enemy.height/2 + AImargin) enemy.y += enemySpeedY;
                }
                
                if (enemy.y < topMargin+5)
                {
                    enemy.y = topMargin+5;
                }
                if (enemy.y > screenHeight - enemy.height-limiteCampo)
                {
                    enemy.y = screenHeight - enemy.height-limiteCampo;
                }
                
                // TODO: Collision detection (ball-player) logic.....(0.5p)
                if (CheckCollisionCircleRec(ballPosition, ballRadius, player) && ballSpeed.x < 0)
                {
                    PlaySound(collisionSound);
                    ballSpeed.x *= -1;                   
                }

                // TODO: Collision detection (ball-enemy) logic......(0.5p)
                if (CheckCollisionCircleRec(ballPosition, ballRadius, enemy) && ballSpeed.x > 0)
                {
                    PlaySound(collisionSound);
                    ballSpeed.x *= -1;                    
                }

                
                // TODO: Collision detection (ball-limits) logic.....(1p)
                    //límite arriba
                if (ballPosition.y - ballRadius < topMargin+limiteCampo && ballSpeed.y < 0) //cambiar cuando esté la textura personalizada
                {
                    ballSpeed.y *= -1;
                }
                    //límite abajo
                  if (ballPosition.y + ballRadius > screenHeight-limiteCampo && ballSpeed.y > 0)
                {
                    ballSpeed.y *= -1;
                }  
                    //límite izquierda
                  if (ballPosition.x - ballRadius < limiteCampo && ballSpeed.x < 0)
                {
                    PlaySound(pointSound);
                    ballSpeed.x *= -1;
                    playerLife--;
                    // TODO: Player life bar decrease logic....................(1p)
                    playerLifeRec.width -= widthDamage;
                    
                }  
                    //límite derecha
                  if (ballPosition.x + ballRadius > screenWidth-limiteCampo && ballSpeed.x > 0)
                {
                    PlaySound(pointSound);
                    ballSpeed.x *= -1;
                    enemyLife--;
                    // TODO: Enemy life bar decrease logic....................(1p)
                    enemyLifeRec.width -= widthDamage;
                    enemyLifeRec.x += widthDamage;
                    
                }  
                
                               

                // TODO: Time counter logic..........................(0.2p)
                framesCounter++;
                if (framesCounter % 60 == 0)
                {
                    framesCounter = 0;
                    secondsCounter--;
                }
                
                // TODO: Game ending logic...........................(0.2p)

                    if (secondsCounter <= 0)
                    {
                        if (playerLife < enemyLife){                                                      
                            gameResult = 0;            
                              
                        }
                        else if (playerLife > enemyLife){                                                       
                            gameResult = 1;                             
                        }
                        
                        else {
                            PlaySound(empateSound);
                            gameResult = 2;
                        }

                    }
                    else if (playerLife <= 0) gameResult = 0;
                    else if (enemyLife <= 0) gameResult = 1;

                    
                    //else if (playerLife < enemyLife) PlaySound(loseSound);
                    //else if (playerLife > enemyLife) PlaySound(winSound);
                    
                    if (gameResult != -1)
                    {
                        StopMusicStream(gameplayMusic);
                        screen = ENDING;
                        if (gameResult = 0) PlaySound(loseSound);
                        else if (gameResult = 1) PlaySound(winSound);
                    }
                    
                    
               
             }
             
                //else if (gameResult = 0);{
                            //PlaySound(loseSound);
                        //}
                        //else if (gameResult = 1);{
                            //PlaySound(winSound);
                        //}
                
                // TODO: Pause button logic..........................(0.2p)
                if (IsKeyPressed(KEY_P))
                {
                    pausa = !pausa;
                }
                
            } break;
            case ENDING: 
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................(0.5p)
                if (IsKeyPressed(KEY_ENTER))
                {
                    ballPosition = (Vector2) {screenWidth/2 - ballRadius, screenHeight/2 + ballRadius};
                    ballSpeed = (Vector2) { GetRandomValue(3,5), GetRandomValue (4,5)};
                    if(GetRandomValue(0, 1)) ballSpeed.x *= -1;
                    if(GetRandomValue(0, 1)) ballSpeed.y *= -1;
                    
                    player.y = screenHeight/2 - playerHeight/2+25;
                    enemy.y = player.y;
                    
                    playerLife = 5;
                    enemyLife = 5;
                    
                    pausa = false;
                    
                    gameResult = -1;
                    
                    secondsCounter = 99;
                    framesCounter = 99;
                    
                    PlaySound(enterSound);
                    
                    playerLifeRec = playerFillRec;
                    enemyLifeRec =enemyFillRec;
                    PlayMusicStream(gameplayMusic);
                    screen = GAMEPLAY;
                }
                
                
            } break;
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(screen) 
            {
                case LOGO: 
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................(0.2p)
                    DrawTexture(logo, screenWidth/2 - logo.width/2, screenHeight/2 - logo.height/2, Fade(WHITE, alpha));
                    
                } break;
                case TITLE: 
                {
                    // Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................(0.2p)
                    DrawTexture(pantInicio, screenWidth/2 - pantInicio.width/2, screenHeight/2 - pantInicio.height/2, WHITE);
                     
                    DrawTextEx(arcadeFont, titleText, titlePos, titleSize, 0, RED);

                    // TODO: Draw "PRESS ENTER" message..............(0.2p)
                    if (blink) DrawText(startText, screenWidth/2 - MeasureText(startText, 20)/2, screenHeight - 100, 20, WHITE);
                    
                } break;
                case GAMEPLAY:
                { 
                    // Draw GAMEPLAY screen here!
                    
                   
                    DrawTexture(campo,0,50,WHITE);
                    
                    DrawCircleV(ballPosition, ballRadius, ballColor);
                    
                    DrawRectangleRec(mid, BLACK);
                    
                    // TODO: Draw player and enemy...................(0.2p)
                    DrawRectangleRec(player, playerColor);
                    DrawRectangleRec(enemy, playerColor);
                    
                    // TODO: Draw player and enemy life bars.........(0.5p)
                    DrawRectangleRec(playerBgRec, BLACK);
                    DrawRectangleRec(playerFillRec, ballColor);
                    DrawRectangleRec(playerLifeRec, playerLifeColor);
                    
                    DrawRectangleRec(enemyBgRec, BLACK);
                    DrawRectangleRec(enemyFillRec, ballColor);
                    DrawRectangleRec(enemyLifeRec, playerLifeColor);
                    // TODO: Draw time counter.......................(0.5p)
                    DrawText(FormatText("%i", secondsCounter), screenWidth/2 - MeasureText(FormatText("%i", secondsCounter),40)/2, topMargin - 43, 40, playerColor);
                    
                    // TODO: Draw pause message when required........(0.5p)
                    if (pausa)
                    {
                    DrawRectangle(0, 0, screenWidth, screenHeight, Fade(WHITE, 0.8f));
                    DrawText("PAUSA", screenWidth/2 - MeasureText("PAUSA", 30)/2, screenHeight/2-15, 30, BLACK);
                    }
                    
                } break;
                case ENDING: 
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or lose)......(0.2p)
                    if (gameResult == 0) DrawTexture(loser, 0, 0, WHITE);
                    else if (gameResult == 1) DrawTexture(win, 0, 0, WHITE);
                    else if (gameResult == 2) DrawTexture(empate, 0, 0, WHITE);
                    
                    //poner volver a jugar o salir en textura con el resto
                        
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    UnloadTexture(logo);
    UnloadTexture(pantInicio);
    UnloadTexture(campo);
    UnloadTexture(win);
    UnloadTexture(loser);
    UnloadTexture(empate);
    UnloadFont(arcadeFont);
    UnloadSound(enterSound);
    UnloadSound(pointSound);
    UnloadSound(collisionSound);
    UnloadSound(winSound);
    UnloadSound(empateSound);
    UnloadSound(loseSound);
    UnloadMusicStream(gameplayMusic);
    
    CloseAudioDevice();
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}